/*
                                        Consumer Functional Interface Example
        Consumer represents an operation which takes an argument and returns nothing.
         Use this functional interface If you want to compose a lambda expression which performs some operations on an object.
        For example, displaying all students with their specialization.
        Lambda expression implementing Consumer : Displaying all students with their specialization.
*/
package FunctionalInterfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Example2 {
    public static void main(String[] args) {
        //        Create a list and Add students data
        List<Student> listOfStudents = new ArrayList<>();
        listOfStudents.add(new Student("John", "Mathematics"));
        listOfStudents.add(new Student("Harsha", "History"));
        listOfStudents.add(new Student("Ruth", "Computers"));
        listOfStudents.add(new Student("Aroma", "Mathematics"));
        listOfStudents.add(new Student("Zade", "Computers"));
        //       Lambda expression implementing Consumer
        Consumer<Student> studentConsumer = student -> System.out.println(student.name + " specialization is : " + student.specialization);

        listOfStudents.forEach(student -> {
            //    the accept method accepts a single argument and returns nothing.
            studentConsumer.accept(student);
        });
    }
}
