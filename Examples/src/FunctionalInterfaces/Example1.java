/*
                                        Predicate Functional Interface Example
    Predicate represents an operation which takes an argument T and returns a boolean.
    Use this functional interface, if you want to define a lambda expression which performs some test on an argument
    and returns true or false depending upon outcome of the test.
*/


package FunctionalInterfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Example1 {
    public static void main(String[] args) {
//        Create a list and Add students data
        List<Student> listOfStudents = new ArrayList<>();
        listOfStudents.add(new Student("John", "Mathematics"));
        listOfStudents.add(new Student("Harsha", "History"));
        listOfStudents.add(new Student("Ruth", "Computers"));
        listOfStudents.add(new Student("Aroma", "Mathematics"));
        listOfStudents.add(new Student("Zade", "Computers"));

//        Use predicate to create list of student with specialization Mathematics
        Predicate<Student> mathematicsPredicate = student -> student.specialization.equals("Mathematics");
        List<Student> listOfStudentsWithMathematicsSpecialization = new ArrayList<>();

        listOfStudents.forEach(student -> {
//            the test method accepts a single argument and returns either true or false based on predicate
            if (mathematicsPredicate.test(student)) {
                listOfStudentsWithMathematicsSpecialization.add(student);
            }
        });
        System.out.println(listOfStudentsWithMathematicsSpecialization);
    }
}
