package InterfaceChanges;

public class Example3 implements Interface1, Interface3 {
    @Override
    public void display() {
        Interface3.super.display();
    }

    public static void main(String[] args) {
        new Example3().display();
    }
}
