package InterfaceChanges;

interface Interface2 extends Interface1 {
    @Override
    default void display() {
        System.out.println("Display from Interface2");
    }
}
