//Lab4 - Create interface Connection with two methods - open and close
//            with open method as default 
//       create interface Transaction with two methods - open and commit
//            with open method as default
//        Create OracleCon to implement Connection 
//        Create OracleTrans to implement Transaction
//        Main method to create instances and invoke....
//        Create a class Test implementing Connection and Transaction both  
//            Observe messages and error 
//            Rectify 
//            Why?

interface Connection{
public default void open() {
System.out.println("Connection Interface with default Open Method");
}
public void close();
}
interface Transaction{
public default void open() {
System.out.println("Transaction Interface with default Open Method");
}
public void commit();
}
class OracleCon implements Connection{
@Override
public void close() {
System.out.println("OracleCon class with close method");
}
}
class OracleTrans implements Transaction{
@Override
public void commit() {
System.out.println("OracleTrans class with commit method");
}
@Override
public void open() {
System.out.println("OracleTrans class with override open method");
}
}
class Test implements Connection,Transaction{
    	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Transaction t1=new Transaction() {
			
			@Override
			public void commit() {
				// TODO Auto-generated method stub
				System.out.println("in test commit method of transaction");
			}
		};
		t1.open();
		t1.commit();
		Connection cn=new Connection() {
			
			@Override
			public void close() {
				// TODO Auto-generated method stub
				System.out.println("in test close method of connection");

			}
		};
		cn.open();
		cn.close();
	}


	@Override
	public void close() {
		// TODO Auto-generated method stub
		System.out.println("test overddien close method");
	}


	@Override
	public void commit() {
		// TODO Auto-generated method stub
		System.out.println("test overddien commit method");

	}


	@Override
	public void open() {
		// TODO Auto-generated method stub
		Transaction.super.open();
	}
}
public class Example5 {
public static void main(String[] args) {
Connection oracleCon=new OracleCon();
oracleCon.open();
oracleCon.close();
Transaction oracleTrans =new OracleTrans();
oracleTrans.open();
oracleTrans.commit();
}
}