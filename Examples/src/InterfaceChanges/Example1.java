//Rule 1
package InterfaceChanges;

public class Example1 extends class1 implements Interface1, Interface2 {
    public static void main(String[] args) {
        new Example1().display();
    }
}

class class1 implements Interface1, Interface2 {
    @Override
    public void display() {
        System.out.println("Display from class1");
    }
}