/*
                                        Supplier Functional Interface Example
    Supplier represents an operation which takes no argument and returns the results of type R.
     Use this functional interface when you want to create new objects.
*/


package FunctionalInterfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class Example4 {
    public static void main(String[] args) {
        //        Create a list and Add students data
        List<Student> listOfStudents = new ArrayList<>();
        listOfStudents.add(new Student("John", "Mathematics"));
        listOfStudents.add(new Student("Harsha", "History"));
        listOfStudents.add(new Student("Ruth", "Computers"));
        listOfStudents.add(new Student("Aroma", "Mathematics"));
        listOfStudents.add(new Student("Zade", "Computers"));
        Supplier<Student> studentSupplier = () -> new Student("pooja", "Mathematics");
        //      Doesn't accepts any argument but returns a object
        listOfStudents.add(studentSupplier.get());
        System.out.println(listOfStudents);
    }
}

