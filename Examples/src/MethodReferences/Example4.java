package MethodReferences;

import java.util.function.Supplier;

public class Example4 {
    public static void main(String[] args) {
        //Creating objects using lambda
        Supplier<Company1> lambdaSupplier = () -> new Company1();
        lambdaSupplier.get();

        //Creating objects using constructor references
        Supplier<Company1> referenceSupplier = Company1::new;
        referenceSupplier.get();
    }
}

class Company1 {

}
