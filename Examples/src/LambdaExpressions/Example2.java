//Lambda expression with Collection and Stream Api

package LambdaExpressions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Example2 {
    public static void main(String[] args) {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(2000, "abc"));
        list.add(new Product(1000, "xyz"));
        list.add(new Product(500, "mno"));
        List<Product> filteredListOfProducts = list.stream().filter(product -> product.price > 500).collect(Collectors.toList());
        System.out.println(filteredListOfProducts);
    }
}

class Product {
    int price;
    private String name;

    Product(int price, String name) {
        this.price = price;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Product{" + "price=" + price + ", name='" + name + '\'' + '}';
    }
}