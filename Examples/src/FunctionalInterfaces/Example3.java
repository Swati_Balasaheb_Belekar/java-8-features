/*
                                        Function Functional Interface Example
        Function represents an operation which takes an argument of type T and returns a result of type R.
        Use this functional interface if you want to extract some data from an existing data.
        For example,extracting only the names from listOfStudents.
        Lambda expression implementing Function:Extracting only the names of all students*/
package FunctionalInterfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Example3 {
    public static void main(String[] args) {
        //        Create a list and Add students data
        List<Student> listOfStudents = new ArrayList<>();
        listOfStudents.add(new Student("John", "Mathematics"));
        listOfStudents.add(new Student("Harsha", "History"));
        listOfStudents.add(new Student("Ruth", "Computers"));
        listOfStudents.add(new Student("Aroma", "Mathematics"));
        listOfStudents.add(new Student("Zade", "Computers"));
        Function<Student, String> studentFunction = student -> student.name;
        List<String> studentNames = new ArrayList<>();
        listOfStudents.forEach(student -> {
//            Accepts a single argument and returns a object
            studentNames.add(studentFunction.apply(student));
        });
        System.out.println(studentNames);
    }
}

