//Lambda expression with functional interface
package LambdaExpressions;

import java.util.ArrayList;
import java.util.Comparator;

public class Example1 {
    public static void main(String[] args) {
//        BEFORE JAVA 8 USING ANONYMOUS INNER CLASS
        ArrayList<Student> arrayToSortByAnonymousClass = new ArrayList<>();
        arrayToSortByAnonymousClass.add(new Student("Mayank"));
        arrayToSortByAnonymousClass.add(new Student("pranjal"));
        arrayToSortByAnonymousClass.add(new Student("Anshul"));
        arrayToSortByAnonymousClass.sort(new SortByNameUsingAnonymousClass());
        // Collections.sort(arrayToSortByAnonymousClass, new SortByNameUsingAnonymousClass());
        System.out.println(arrayToSortByAnonymousClass);

//        After JAVA 8 USING LAMBDA EXPRESSIONS
        ArrayList<Student> arrayToSortByLambdaExpression = new ArrayList<>();
        arrayToSortByLambdaExpression.add(new Student("priya"));
        arrayToSortByLambdaExpression.add(new Student("daksha"));
        arrayToSortByLambdaExpression.add(new Student("deepa"));
        arrayToSortByLambdaExpression.sort(Comparator.comparing(student -> student.name));
        //Collections.sort(arrayToSortByLambdaExpression, (s1, s2) -> s1.name.compareTo(s2.name));
        System.out.println(arrayToSortByLambdaExpression);
    }
}

//Student Class
class Student {
    String name;

    Student(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }
}

//
class SortByNameUsingAnonymousClass implements Comparator<Student> {
    // Sorting in ascending order of name
    @Override
    public int compare(Student a, Student b) {
        return a.name.compareTo(b.name);
    }
}