// Interface2 extends Interface1 then Interface1 is more specific than Interface2.
package InterfaceChanges;

public class Example2 implements Interface2, Interface1 {
    public static void main(String[] args) {
        new Example2().display();
    }
}
