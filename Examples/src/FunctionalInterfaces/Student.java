//This student class is used for all functional interfaces examples
package FunctionalInterfaces;

public class Student {
    String name;
    String specialization;

    public Student(String name, String specialization) {
        this.name = name;
        this.specialization = specialization;
    }

    @Override
    public String toString() {
        return "Student{" + "name='" + name + '\'' + ", specialization='" + specialization + '\'' + '}';
    }
}